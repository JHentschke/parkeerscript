import re
import urllib
import http

 #Persoonlijke Gegevens
kenteken = "<KENTEKEN>" 
activeringscode = "<ACTIVERINGSCODE>" 
emailadres = "<email@adres.com>" 

 # COOKIES ACCEPTEREN ##
cj = http.cookiejar.CookieJar()
opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(cj))
opener.addheaders = [('User-agent', 'Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0')]
cj.clear()

starturl = urllib.request.Request('https://www.haarlem.nl/ufs/ufsmain?formid=kenteken_activeren')

if kenteken and activeringscode:
    startresp = opener.open(starturl)




#Get  sessionID (ebz)
html = str(startresp.read())
ebzregx = re.compile('\d_\d{13}')
findebz = ebzregx.search(html)
ebz = findebz.group()

#URLS met aanmeldformulier
aanmeldurl = urllib.request.Request('https://www.haarlem.nl/ufs/BrowserCheck;?ebz='+ebz)
gegevensurl = urllib.request.Request('https://www.haarlem.nl/ufs/ufsmain;?ebz='+ebz)


#Aanmeldformulier openen
aanmeldvalues = {'ebase_JSOK': 'false',
                 'formid': 'kenteken_activeren'}

aanmelddata = urllib.parse.urlencode(aanmeldvalues)
aanmelddata = aanmelddata.encode('utf-8')

if kenteken and activeringscode:
    opener.open(aanmeldurl, aanmelddata)



#Aanmeldformulier invullen (stap 1)
gegevensvalues = {'formid': 'KENTEKEN_ACTIVEREN',
                  'ebs': ebz,
                  'ufsEndUser*': '1',
                  'pageSeq': '1',
                  'pageId': '10',
                  'formStateId': '1',
                  'CTRL:139:_:A': activeringscode,
                  'CTRL:140:_:A': kenteken,
                  'CTRL:141:_:A': emailadres,
                  'CTRL:142:_:A': 'Y',
                  'CTRL:86:_': 'Volgende: controleer en bevestig',
                  'HID:inputs': 'VCTRL:69:_:A,ICTRL:139:_:A,ICTRL:140:_:A,ICTRL:141:_:A,ICTRL:142:_:A,ACTRL:86:_'}

gegevensdata = urllib.parse.urlencode(gegevensvalues).encode('utf-8')
if kenteken and activeringscode:
    opener.open(gegevensurl, gegevensdata)




# Aanmeldformulier bevestigen (stap 2)
controlevalues = {'formid': 'KENTEKEN_ACTIVEREN',
                  'ebs': ebz,
                  'ufsEndUser*': '1',
                  'pageSeq': '2',
                  'pageId': '20',
                  'formStateId': '1',
                  'CTRL:159:_': 'Kenteken aanmelden >',
                  'HID:inputs': 'VCTRL:144:_:A,ACTRL:164:_,ACTRL:159:_,ACTRL:163:_'}

controledata = urllib.parse.urlencode(controlevalues).encode('utf-8')

